import React from "react";
import Layout from "../components/layout";
import SEO from "../components/seo";
import { graphql } from "gatsby";
import { withStyles } from "@material-ui/core/styles";
import CategoryCard from "../components/CategoryCard";

const styles = theme => ({
  root: {
    backgroundColor: theme.palette.background.paper,
  },
  flexBoxParentDiv: {
    display: "flex",
    flexDirection: "row",
    flexWrap: "wrap",
    flexFlow: "row wrap",
    justifyContent: "space-between",
    "&::after": {
      content: "",
      flex: "auto",
    },
  },
})

class CategoryPage extends React.Component {
  render() {
    const classes = this.props.classes

    let items = this.props.data.allGoogleSpreadsheetExport.edges
    let uniqueCategoryWithLength = []
    const categoryMap = new Map()
    for (const item of items) {
      for (const category of item.node.category.split(",")) {
        if (!categoryMap.has(category)) {
          categoryMap.set(category, true)
          uniqueCategoryWithLength.push({
            categoryName: category,
            categoryIcon: item.node.icon,
            numberOfCards: items.filter(
              innerItem => innerItem.node.category.split(',').includes(category) // test if category is in node.category array
            ).length,
          })
        }
      }
    }

    return (
      <Layout>
        <SEO title="Explore services by Category" />
        <div className={classes.root}>
          <div className={classes.flexBoxParentDiv}>
            {uniqueCategoryWithLength.map((category, index) => (
              // add a key index to the mapping of uniqueCategoryWithLegth as mentionned in https://fr.reactjs.org/docs/lists-and-keys.html -- fix Warning: Each child in a list should have a unique "key" prop.
              // and fix duplicate key error
              <CategoryCard category={category} key={index.toString()}/>
            ))}
          </div>
        </div>
      </Layout>
    )
  }
}

export default withStyles(styles)(CategoryPage)

export const query = graphql`
  query CategoryQuery {
    allGoogleSpreadsheetExport {
      edges {
        node {
          id
          optimized_thumbnail {
            childImageSharp {
              fluid(maxWidth: 400, maxHeight: 250) {
                ...GatsbyImageSharpFluid
              }
            }
          }
          title
          category
          icon
          location
          locationImage
          servicesProvided
        }
      }
    }
  }
`
