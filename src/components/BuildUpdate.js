import React from "react";
import axios from "axios"

export default class BuildUpdate extends React.Component {
    constructor(props, response){
      super(props)
      this.state = {
        url: "",
        response: "",
        succeed: '',
        date: ""
  
      };
    }
    componentDidMount() {
      axios.get(`https://gitlab.com/api/v4/projects/${process.env.GATSBY_PIPELINE}/pipelines`, {
        headers: {
        AUTHORIZATION: `PRIVATE-TOKEN: ${process.env.GATSBY_PRIVATETOKEN}`
        }
      })
     // .then(res => console.log(res))
      .then(response => this.setState({
        response: response.data[0].status,
        url: response.data[0].web_url,
        date: response.data[0].updated_at
      }, function(){
        if( response.data[0].status === 'success'){
          this.setState({
            succeed: 'success'
          })
        }
        else if (response.data[0].status === 'running'){
          this.setState({
            succeed: 'running'
          })
        }
        else if (response.data[0].status === 'pending'){
          this.setState({
            succeed: 'pending'
          })
        }
        else {
          this.setState({
            succeed: 'failure'
          })
        }
      }))
  
    }
    render(){
      var date = new Date(this.state.date).toLocaleDateString("en-US")
      var hours = new Date(this.state.date).getHours()
      var minutes = new Date(this.state.date).getMinutes()
      return (
      <div class="buildStep">
      <a class="noStyle" href={this.state.url}>
        <table>
          <tbody className="tbody">
            <tr>
              <div className="horizontAlign">
                <div className="col1">last build</div> 
                <div>
                  <span className={this.state.succeed}>{this.state.response}</span>
                </div>
                <div className="date">
      <span className="dateTime"> Last website update : {date} at {hours} h {minutes} min</span>
              </div>
              </div>
  
            </tr>
          </tbody>
      </table>
    </a>
        </div>
      )
    }
  
  }
