import React from "react"
import { withStyles } from "@material-ui/core/styles"
import Card from "@material-ui/core/Card"
import { Link } from "gatsby"
import { kebabCase } from "lodash"
import CardHeader from "@material-ui/core/CardHeader";
import Badge from "@material-ui/core/Badge";
import Icon from "@material-ui/core/Icon";

const styles = theme => ({
  card: {
    width: 310,
    background: '',
    margin: 10,
    [theme.breakpoints.down('sm')]: {
      width: "98vw",
    },
  }
})

function CategoryCard(props) {
  const { classes, category } = props
  return (
  <Card className={classes.card}>
    <Link style={{ textDecoration: "none" }} to={`/category/${kebabCase(category.categoryName)}/`}>
        <CardHeader
          avatar={
          <Badge badgeContent={category.numberOfCards} color="primary">
          <Icon 
            color="secondary"
            fontSize="large"
          >{category.categoryIcon}</Icon>
          </Badge>
          }
          title={category.categoryName}
          titleTypographyProps={{variant:'h6' }}
        />
    </Link>
  </Card>
  )
}

export default withStyles(styles)(CategoryCard)
