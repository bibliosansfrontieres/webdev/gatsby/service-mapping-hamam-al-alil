const path = require(`path`)
const axios = require("axios")
const _ = require("lodash")

exports.createPages = ({ actions, graphql }) => {
  const { createPage } = actions

  return graphql(`
    {
      allGoogleSpreadsheetExport {
        edges {
          node {
            id
            title
            icon
            category
            thumbnail
            tags
            servicesProvided
            conditions
            location
            locationImage
            contactPerson
            openingTimes
            comments
          }
        }
      }
    }
  `).then(result => {
    if (result.errors) {
      result.errors.forEach(e => console.error(e.toString()))
      return Promise.reject(result.errors)
    }

    const items = result.data.allGoogleSpreadsheetExport.edges

    items.forEach(edge => {
      const id = edge.node.id
      const title = edge.node.title
      const cardPath = `/card/${id}/${_.kebabCase(title)}/`

      createPage({
        path: cardPath,
        component: path.resolve(`src/templates/single-item.js`),
        context: {
          itemId: id,
        },
      })
    })

    // Tag pages:
    let tags = []

    items.forEach(item => {
      if (item.node.tags.length > -1) {
        tags = tags.concat(item.node.tags.split(","))
      }
    })

    tags = _.uniq(tags)

    tags.forEach(tag => {
      const tagPath = `/tag/${_.kebabCase(tag)}/`

    //tag = `/${_.kebabCase(tag)}/`
    let tag_trimmed = tag.trim()
    tag = "/" + tag_trimmed.toLowerCase() + "/"

      createPage({
        path: tagPath,
        component: path.resolve(`src/templates/single-tag.js`),
        context: {
          tag,
        },
      })
    })

    // Caterogy pages:
    let categories = []

    items.forEach(item => {
      if (item.node.category.length > -1) {
        categories = categories.concat(item.node.category)
      }
    })

    categories = _.uniq(categories)

    categories.forEach(category => {
      const categoryPath = `/category/${_.kebabCase(category)}/`

      createPage({
        path: categoryPath,
        component: path.resolve(`src/templates/single-category.js`),
        context: {
          category,
        },
      })
    })

    let locations = []

    items.forEach(item => {
      if (item.node.location.length > -1) {
        locations = locations.concat(item.node.location)
      }
    })

    locations = _.uniq(locations)

    locations.forEach(location => {
      const locationPath = `/location/${_.kebabCase(location)}/`

      createPage({
        path: locationPath,
        component: path.resolve(`src/templates/single-location.js`),
        context: {
          location,
        },
      })
    })
  })
}
